#ex -sc '%s/.*/"&",/|x' file1.txt including qoma
#https://www.onlinedoctranslator.com

##################################################################
#   GO THROUGH TS FILES INSIDE APP FOLDER AND CALL SED COMMAND
##################################################################


gettsfilesandreplace(){
local ORIGINAL=$1;
local REPLACE=$2;

ls -d -1 "../app/"**/* |
 { while read i
 do 
	
	if [[ "$i" == ../app/Templates/* ]] || [[ "$i" == ../app/lang/* ]];then
		
		   continue
	fi
	
    if [ -d "$i" ];then
        ls -d -1 "$i"/* |
        { while read k
        do
            if [ -s "$k" ]
                then
                sed -i "s/${ORIGINAL}/${REPLACE}/g" "$k"
            fi
        done
        }
    fi
 
	if [ -s "$i" ]
		then 
		   sed -i "s/${ORIGINAL}/${REPLACE}/g" "${i}"
		else
		   continue
	fi
	
done

}
}
alias getts=gettsfilesextract;

########################################################################
#   GO THROUGH HBS FILES INSIDE TEMPLATES FOLDER AND CALL SED COMMAND
########################################################################

gethbsfilesandreplace(){
    local ORIGINAL=$1;
    local REPLACE=$2;
ls -d -1 "../app/Templates/theme/adminty/"**/* |
 { while read i
 do 
    if [ -s "$i" ]
    then 
        sed -i "s/${ORIGINAL}/${REPLACE}/g" "${i}"
     else
        continue
    fi
done

}
}
alias gethbs=gethbsfilesextract;


#############################################################
#  REPLACES STRING USING SED COMMAND
#############################################################

# -o original string to be replaced
# -r replace string
# -f single file path
# -t replace it on all typescript files inside app
# -h replace it on all hbs files inside Template

replacestring(){
unset OPTIND
while getopts o:r:f:th OPTION
do
    case ${OPTION} in 
        o)
        local ORIGINAL="${OPTARG}"
        ;;
        r)
        local REPLACE="${OPTARG}"
        ;;
        f)
        local FILEPATH="${OPTARG}"
        ;;
        h)
        local REPLACEFORALLHBS=true
        ;;
        t)
        local REPLACEFORALLTS=true
        ;;
        ?) # for single char unlike *
        echo 'invalid'
        ;;
    esac
done

if [ -z ${REPLACE+x} ]; then echo "Please provide REPLACE value";return; fi
if [ -z ${ORIGINAL+x} ]; then echo "Please provide ORIGINAL value";return; fi
if [ -z ${FILEPATH+x} ] && [ -z ${REPLACEFORALLHBS+x} ] && [ -z ${REPLACEFORALLTS+x} ]
then 
    echo "Please provide FILEPATH value or -h or -t"
    return
 fi

if [ ! -z ${FILEPATH+x} ]
then 
    sed -i "s/${ORIGINAL}/${REPLACE}/g" "${FILEPATH}"
    echo "Replace of ${ORIGINAL} to ${REPLACE} into ${FILEPATH} is done."
    return
 fi

if [ ! -z ${REPLACEFORALLHBS+x} ]
then 
    gethbsfilesandreplace ${ORIGINAL} ${REPLACE}
    echo "Replace of ${ORIGINAL} to ${REPLACE} into all hbs files is done."
    return
 fi

 if [ ! -z ${REPLACEFORALLTS+x} ]
then 
    gettsfilesandreplace ${ORIGINAL} ${REPLACE}
    echo "Replace of ${ORIGINAL} to ${REPLACE} into ts files is done."
    return
 fi

}
alias replacestring=rs;

#############################################################
#  GET HBS FILES AND CALL getstringbetweeni18nblock
#############################################################

# ls -d -l lists all files and folders with paths line by line
# while loop on the lines
# check if it is a file because folders could be included in the lines
# on each line or file perform getstringbetweeni18nblock

gethbsfilesextract(){
ls -d -1 "../app/Templates/theme/adminty/"**/* |
 { while read i
 do 
 
	if [ -s "$i" ]
then 
   getstringbetweeni18nblock "${i}";
else
   continue
fi
	
done

}
}
alias gethbs=gethbsfilesextract;

#############################################################
#  GET HBS FILES AND CALL getstringbetweeni18nblock
#############################################################

# ls -d -l lists all files and folders with paths line by line
# while loop on the lines
# check if it is a file because folders could be included in the lines
# on each line or file perform getstringbetweeni18nblock

gettsfilesextract(){
ls -d -1 "../app/"**/* |
 { while read i
 do 
	
	if [[ "$i" == ../app/Templates/* ]] || [[ "$i" == ../app/lang/* ]];then
		
		   continue
	fi
	
    if [ -d "$i" ];then
    ls -d -1 "$i"/* |
    { while read k
    do
    if [ -s "$k" ]
    then
    getstringbetweeni18nhelperfunction "$k"
    fi
    done
    }
    fi
 
	if [ -s "$i" ]
		then 
		   getstringbetweeni18nhelperfunction "${i}"
		else
		   continue
	fi

   
	
done

}
}
alias getts=gettsfilesextract;


#############################################################
#  GET TEXT FROM i18n HBS BLOCK FUNCTION
#############################################################

# extract text in between {{#i18n}}{{/i18n}}
# read the file by char and loop through it
# IFS='' does not ignore the spaces
# so {{#i18n}} so I put up flags which increments on every char hit
# if reaches 9 then we begin to get the text inside
# if {{/i18n}} then we start to decrement to 0 so we now we are outside the block
# remove the emtpy lines with awk '{$1=$1;print}'
# remove trailing spaces sed '/^[[:space:]]*$/d'

getstringbetweeni18nblock(){

f=0;
line=""
local file=$1

while IFS='' read -r -n 1 i
do

 case $i in 
 {)
    if [ $f -eq 0 ] || [ $f -eq 1 ]; then
        f=$(($f+1))
    fi
    if [ $f -eq 9 ] || [ $f -eq 8 ]; then
        f=$(($f-1))
    fi
continue ;;

"/")
    if [ $f -eq 7 ]; then
        f=$(($f-1))
    fi
    if [ $f -eq 9 ]; then
        line+="$i"
        fi
continue ;;

"\\")
    if [ $f -eq 9 ]; then
        line+="$i"
    fi
continue ;;

"#")
    if [ $f -eq 2 ]; then
        f=$(($f+1))
    fi
    if [ $f -eq 9 ]; then
        line+="$i"
        fi
continue ;;

i)
    if [ $f -eq 3 ]; then
        f=$(($f+1))
    fi
    if [ $f -eq 6 ]; then
        f=$(($f-1))
      
    fi
    if [ $f -eq 9 ]; then
        line+="$i"
        fi
continue ;;

1)
    
     if [ $f -eq 4 ]; then
        f=$(($f+1))

    elif [ $f -eq 5 ]; then
    f=$(($f-1))
    fi

    if [ $f -eq 9 ]; then
        line+="$i"
    fi
continue ;;

8)
    if [ $f -eq 5 ]; then
        f=$(($f+1))
    
    elif [ $f -eq 4 ]; then
        f=$(($f-1))
    fi
    if [ $f -eq 9 ]; then
        line+="$i"
        fi
continue ;;

n)
   
    if [ $f -eq 6 ]; then
        f=$(($f+1))
       
    fi

    if [ $f -eq 3 ]; then
        f=$(($f-1))
     
    fi
      
    
     if [ $f -eq 9 ]; then
        line+="$i"
        fi
  continue ;;

  "}")
    if [ $f -eq 7 ] || [ $f -eq 8 ]; then
        f=$(($f+1))
    fi
    if [ $f -eq 2 ] || [ $f -eq 1 ]; then
        f=$(($f-1))
        echo "${line}"
        line=""  
    fi
  continue ;;
  
  [[:space:]])
  if [ $f -eq 9 ]; then
  line+=" "
    fi
 continue ;;

 esac

if [ $f -eq 9 ]; then
    line+="$i"
fi

done < $file | awk '{$1=$1;print}' | sed '/^[[:space:]]*$/d' >> eng.txt;
}
alias drilli18n=getstringbetweeni18nblock

#############################################################
#  GET TEXT FROM i18n HELPER FUNCTION
#############################################################

# extract text in between i18n()
# read the file by char and loop through it
# IFS='' does not ignore the spaces
# so i18n( so I put up flags which increments on every char hit
# if reaches 5 then we begin to get the text inside
# if ) then it becomes 0 so we now we are outside the function
# remove the emtpy lines with awk '{$1=$1;print}'
# remove trailing spaces sed '/^[[:space:]]*$/d'

getstringbetweeni18nhelperfunction(){
f=0;
line=""
while IFS='' read -r -n 1 i
do

 case $i in 
 
 
 i)
    if [ $f -eq 0 ]; then
        f=$(($f+1))
    fi

    if [ $f -eq 5 ]; then
        line+="$i"
    fi
   
continue ;;

1)
    
     if [ $f -eq 1 ]; then
    f=$(($f+1))
    fi
    if [ $f -eq 5 ]; then
        line+="$i"
    fi
    continue ;;

8)
    if [ $f -eq 2 ]; then
        f=$(($f+1))
    fi
    if [ $f -eq 5 ]; then
        line+="$i"
    fi
continue ;;

n)
   
    if [ $f -eq 3 ]; then
        f=$(($f+1))
       
    fi
      
    
     if [ $f -eq 5 ]; then
        line+="$i"
        fi
  continue ;;
 
 "(")
    if [ $f -eq 4 ]; then
        f=$(($f+1))
    fi
continue ;;




  ")")
    if [ $f -eq 5 ]; then
        f=0
		echo "${line}"
        line="" 
    fi
	 
  continue ;;
  
  [[:space:]])
  if [ $f -eq 5 ]; then
  line+=" "
    fi
 continue ;;

 "\\")
    if [ $f -eq 9 ]; then
        line+="$i"
    fi
continue ;;

 esac

  if [ $f -eq 5 ]; then
  line+="$i"
 fi

done < $1 | awk '{$1=$1;print}' | sed '/^[[:space:]]*$/d' >> eng.txt;
}
alias drilli18nfunction=getstringbetweeni18nhelperfunction


##############################################################################################
# COMBINES LINES OF 2 FILES INTO 1 AND THEN CONVERTS TO JSON FORMAT FILE var object = {"":""}
##############################################################################################


# copy the txt files into temps
# add "" to the lines of both the files
# then merge the lines of both files containing ""
# paste the above into temp.txt
# create the json file by adding var en_es = {'temp.txt content'};
# delete the temps.
combine2lines(){
cp eng.txt temp1.txt
cp eng.en.es.txt temp2.txt
ex -sc '%s/.*/"&"/|x' temp1.txt
ex -sc '%s/.*/"&",/|x' temp2.txt
paste -d ":" temp1.txt temp2.txt > temp.txt
sed -e '1s/^/var en_es = {/' -e '$s/,$/}/' temp.txt > en_es.txt
mv -- en_es.txt "$(basename -- "en_es" .txt).js"
rm temp.txt
rm temp1.txt
rm temp2.txt		
}
alias c2l=combine2lines


removeDuplicateLines(){
local fileName=$1;
#sed '$!N; /^\(.*\)\n\1$/!P; D' $1 > patch.txt
#cp patch > $fileName;
#rm patch;
awk '{if (++dup[$0] == 1) print $0;}' $fileName > patch
cp patch $fileName;
rm patch;
}
alias rdl=removeDuplicateLines




################################################################
# (WIP) TRYING A BETTER APPROACH OF EXTRACTION
################################################################
test3(){ #hello
var1="abc{{#i18n}}abc{{/i18n}}"
echo "var1 = $var1"
t=${var1#*\{\{\#i18n\}\}}
two=${t%\{\{\/i18n\}\}*}
echo "$t"
echo $two

}
alias test3=test3

################################################################
# REMOVE DOUBLE QUOTES FROM ALL LINES
################################################################

removeduplequotes(){
    sed 's/"//g' $1 > patch
    cp patch $1
}
alias rdq=removeduplequotes

function listfunctionsinscript() # Show a list of functions
{
    echo "functions available:"
    cat .bash_profile | awk '/ \(\) $/ && !/^_/ {print $1}'
}
alias lfs=listfunctionsinscript

#####################################################
# GET ALL FUNCTIONS IN THE CURRENT SCRIPT
#####################################################

getallfunctionsinscript(){
typeset -f | awk '/ \(\) $/ && !/^_/ {print $1}'
#declare -F | awk '{print $NF}' | sort | egrep -v "^_" 
}
alias getallfunc=getallfunctionsinscript


# function functionaliaslist() {
#     echo
#     echo -e "\033[1;4;32m""Functions:""\033[0;34m"
#     declare -F | awk {'print $3'}
#     echo
#     echo -e "\033[1;4;32m""Aliases:""\033[0;34m"
#     alias | awk {'print $2'} | awk -F= {'print $1'}
#     echo
#     echo -e "\033[0m"
# }
# alias test=functionaliaslist
